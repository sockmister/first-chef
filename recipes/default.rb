#
# Cookbook Name:: rails
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
package 'git'

# directory "~/.rbenv" do
#     recursive true
#     action :create
# end

git "/home/vagrant/.rbenv" do
    user "vagrant"
	repository "https://github.com/sstephenson/rbenv.git"
	reference "master"
	action :sync
end

bash "export_path" do 
    user "vagrant"
    code <<-EOF
        echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
        echo 'eval "$(rbenv init -)"' >> ~/.bashrc
        source ~/.bashrc
    EOF
end

directory "/home/vagrant/.rbenv/plugins" do
    user "vagrant"
    recursive true
    action :create
end

git "/home/vagrant/.rbenv/plugins/ruby-build" do
    user "vagrant"    
    repository "https://github.com/sstephenson/ruby-build.git"
    reference "master"
    action :sync
end

%w{autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev libsqlite3-dev nodejs}.each do |pkg|
    package pkg do
        action :install
    end
end

bash "rbenv_install" do
    user "vagrant"
    code <<-EOF
        . ~/.bashrc
        /home/vagrant/.rbenv/bin/rbenv install 2.2.0 --skip-existing
        /home/vagrant/.rbenv/bin/rbenv global 2.2.0
    EOF
end

bash "rails" do
    user "vagrant"
    code <<-EOF
        export PATH="$HOME/.rbenv/bin:$PATH"
        eval "$(rbenv init -)"
        gem install rails
    EOF
end

git "/home/vagrant/proj" do
    user "vagrant"
    repository "https://sockmister@bitbucket.org/sockmister/rails-new.git"
    reference "master"
    action :sync
end

bash "run rails" do
    user "vagrant"
    code <<-EOF
        export PATH="$HOME/.rbenv/bin:$PATH"
        eval "$(rbenv init -)"
        cd /home/vagrant/proj
        rails s -d
    EOF
end

# TODO
# put rails behind nginx
package "nginx" do
    action :install
end

service "nginx" do
    action :enable
end
# use a file template for server.conf
# fill up template using attributes etc.

# provision postgresql
